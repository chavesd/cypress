# [EN] Cypress
Tests using cypress.io
## [EN] Requirements
* Node.js
* Visual Studio Code or other editor
* Git
* Git Bash
## [EN] Installation Steps / Usage
1. Use Git to pull the repository 
2. Open the project in Visual Studio Code
3. Open Git Bash and cd into the project directory
4. Run ```npm install``` to install dependencies
5. Run ```npm run open``` to execute Cypress

----


# [PT] Cypress
Testes à ferramenta cypress.io

## [PT] Requisitos
* Node.js
* Visual Studio Code ou outro editor
* Git
* Git Bash
## [PT] Instalação / Utilização
1. Utilize o git para fazer pull do repositório 
2. Abra o projeto no Visual Studio Code
3. Abra o Git Bash e faça ```cd``` até ao diretório do projeto
4. Corra ```npm install``` para instalar as dependencias
5. Corra ```npm run open``` para executar o Cypress
