// ***********************************************************
// This example support/index.js is processed and
// loaded automatically before your test files.
//
// This is a great place to put global configuration and
// behavior that modifies Cypress.
//
// You can change the location of this file or turn off
// automatically serving support files with the
// 'supportFile' configuration option.
//
// You can read more here:
// https://on.cypress.io/configuration
// ***********************************************************

// Import commands.js using ES2015 syntax:
import './commands'

// Alternatively you can use CommonJS syntax:
// require('./commands')


//Comando de Utilização do XPATH no Cypress
require('cypress-xpath')

// Comando para criptografar password no Log
    //cy.get('#password').type('superSecret123', { sensitive: true })
    Cypress.Commands.overwrite('type', (originalFn, element, text, options) => {
        if (options && options.sensitive) {
          // turn off original log
          options.log = false
          // create our own log with masked message
          Cypress.log({
            $el: element,
            name: 'type',
            message: '*'.repeat(text.length),
          })
        }
      
        return originalFn(element, text, options)
      })
      
      // Comando de login usando UI
        // cy.typeLogin({ email: 'fake@email.com', password: 'Secret1' })
      Cypress.Commands.add('typeLogin', (user) => {
        cy.get("[type='email']")
            .type(user.email)
            .should('have.value', user.email)
    
        cy.get("[type='password']")
            .type(user.password, { sensitive: true })
      })
    
    
      Cypress.Commands.add('generate_random_string', (string_length) => { 
        let random_string = '';
        let random_ascii;
        for(let i = 0; i < string_length; i++) {
            random_ascii = Math.floor((Math.random() * 25) + 97);
            random_string += String.fromCharCode(random_ascii)
        }
        return random_string
       });
    

       import { addMatchImageSnapshotCommand } from 'cypress-image-snapshot/command';

addMatchImageSnapshotCommand();
      
