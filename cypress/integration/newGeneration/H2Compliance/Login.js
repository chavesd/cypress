/// <reference types="cypress" />

describe('Login', () => {

    it('Login Valid', () => {

    //URL
    cy.visit('https://quality.circul8.world/login/eb0f90745a3ec69921314fgscas')
    cy.title()
        .should('be.equal', 'circul8® - Home of Circular Economy Efficiency')
    //Login - Email / Passoword
    cy.typeLogin({ email: 'n@prod.com', password: '123' })
    //Button "Submit"
    cy.get(':nth-child(1) > .button')
        .click()
    // Validar página de Login
    cy.get('.level-item > .title')
        .should('have.text', (' Dashboard '))

    })
    
})

