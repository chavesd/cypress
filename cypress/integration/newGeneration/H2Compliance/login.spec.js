/// <reference types="cypress" />

before(() => {

    cy.visit('https://quality.circul8.world/login/eb0f90745a3ec69921314fgqqde')
    cy.title()
        .should('be.equal', 'circul8® - Home of Circular Economy Efficiency')
        .and('contain', 'Home of Circular Economy Efficiency') 

})

 describe('Login', () => {

    it('Invalid', () => {

        cy.get("[type='email']")
            .click()
            .clear()
            .type('eb0f90745a3ec69921314fgqqde@prodigentia.com')
            .should('have.value', 'eb0f90745a3ec69921314fgqqde@prodigentia.com')

        cy.get("[type='password']")
            .clear()
            .type('12545')
    
        cy.get(':nth-child(1) > .button').click()    

    })

    it('Email Validation', () => {

        cy.get("[type='email']")
            .click()
            .clear()
            .type('aaaaa.com')
            .should('have.value', 'aaaaa.com')

        cy.get("[type='password']")
            .clear()
            .type('123')

        cy.get(':nth-child(1) > .button')
            .click()

        cy.get('.help')
            .should('contain', 'Please include an \'@\' in the email address. \'aaaaa.com\' is missing an \'@\'.')

        cy.get("[type='email']")
            .click()
            .clear()
            .type('aa@@aaa.com')
            .should('have.value', 'aa@@aaa.com')

        cy.get(':nth-child(1) > .button')
            .click()

        cy.get('.help')
            .should('contain', 'A part following \'@\' should not contain the symbol \'@\'.')

    })
 
     it('Login, Logout', () => {

        cy.readFile('C:/Users/chavesd/curso-cypress/cypress/integration/newGeneration/package.variable.json')

        cy.get("[type='email']")
            .click()
            .clear()
            .type('eb0f90745a3ec69921314fgqqde@prodigentia.com')
            .should('have.value', 'eb0f90745a3ec69921314fgqqde@prodigentia.com')

        cy.get("[type='password']")
            .clear()
            .type('123')

        cy.get(':nth-child(1) > .button').click()

        cy.get('.level-item > .title').should('have.text', (' Dashboard '))

        cy.get('.is-right > .navbar-item > .icon > .mdi')
            .click()
        cy.get('.is-desktop-icon-only > :nth-child(2)')
            .click()
        cy.get(':nth-child(1) > .button')
            .should('exist', 'Login')
    }) 

})
 

