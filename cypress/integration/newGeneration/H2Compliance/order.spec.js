/// <reference types="cypress" />

before(() => {

    cy.visit('https://quality.circul8.world/login/eb0f90745a3ec69921314fgqqde')
    cy.title()
        .should('be.equal', 'circul8® - Home of Circular Economy Efficiency')
        .and('contain', 'Home of Circular Economy Efficiency')

})


describe('Order', () => {

    beforeEach(() => {

        cy.get("[type='email']")
            .click()
            .clear()
            .type('eb0f90745a3ec69921314fgqqde@prodigentia.com')
            .should('have.value', 'eb0f90745a3ec69921314fgqqde@prodigentia.com')
    
        cy.get("[type='password']")
            .clear()
            .type('123')
    
        cy.get(':nth-child(1) > .button').click()
    
        cy.get('.level-item > .title').should('have.text', (' Dashboard '))
    
        
    })

    it('Create Order', () => {
        
        cy.get(':nth-child(1) > .is-hidden-desktop > .icon > .mdi')
            .click()

        cy.get(':nth-child(8) > :nth-child(1) > .has-icon > .menu-item-label')
            .should('contain', 'Logistics orders')
            .click()

        cy.contains('Create - EAR request')
            .click()

        cy.url()
            .should('eq', 'https://quality.circul8.world/orders/new')

        cy.get(':nth-child(2) > .form-group > .field-wrap > span > .field > .control > .input')
            .type('Teste458')
            .should('have.value', 'Teste458')

    /*     cy.get(':nth-child(1) > .field-wrap > :nth-child(1) > :nth-child(1) > .datepicker > .dropdown > .dropdown-trigger > .control > .input')
            .type('2020-01-01')
            .should('have.value', '2020-01-01') */
        
        
        cy.get(':nth-child(4) > .field-awesome > .field-wrap > :nth-child(1) > .field > .control > .select > select')
            .select('589')
            .should('have.value', '589')

        cy.get(':nth-child(4) > .field-abstract > .field-wrap > span > .field > .control > .input')
            .type('452145') 
            .should('have.value', '452145')

        cy.get(':nth-child(5) > .form-group > .field-wrap > :nth-child(1) > .field > .control > .select > select')
            .select('586')
            .should('have.value', '586')
        
        cy.get(':nth-child(6) > .form-group > .field-wrap > :nth-child(1) > .field > .control > .select > select')
            .select('431')
            .should('have.value', '431')

        cy.get('#orderitemcontainer-id > .button')
            .click()
        cy.get('#orderitemcontainer-id > .button')
            .should('exist')

    })

})