/// <reference types="cypress" />

before(() => {

    cy.visit('https://test.circul8.world/returns/DmNbyDQ3fpsRhV44j7Zw2cCOJNnjIpCz/pt/en')
    cy.title()
        .should('be.equal', 'circul8® - Home of Circular Economy Efficiency')

})

describe('Order', () => {

    it('Create Order', () => {

        cy.get('[style=""] > .card > .card-content > :nth-child(1) > form > :nth-child(2) > .button')
            .click()
        cy.wait(3000)
        cy.contains('Order Details')
        //Materials
        cy.get('[style=""] > .card > .card-content > :nth-child(1) > form > .vue-form-generator > :nth-child(3) > .form-group > .field-wrap > #orderitems > .button')
            .click()
        //Category - Items to be recycled
        cy.get('[data-cy=ItemsRecycled]').eq(1)
            .select('23')
            .should('have.value', '23')
        //Quantity (Units)
        cy.get('[data-cy=QuantityUnits]').eq(1)
            .type('20')
            .should('have.value', '20')
        //Description
        cy.get('[style=""] > :nth-child(1) > :nth-child(1) > :nth-child(1) > :nth-child(1) > :nth-child(1) > :nth-child(3) > .field-array > :nth-child(2) > #orderitems > :nth-child(1) > :nth-child(1) > #orderitemsc0 > .modal > .animation-content > .card > .card-content > :nth-child(1) > form > :nth-child(1) > :nth-child(1) > .vue-form-generator > fieldset > .field-textarea > .field-wrap > span > .field > .control > .textarea')
            .type('Teste João')
            .should('have.value', 'Teste João')
        //Button Update
        cy.get('[style=""] > :nth-child(1) > :nth-child(1) > :nth-child(1) > :nth-child(1) > :nth-child(1) > :nth-child(3) > .field-array > :nth-child(2) > #orderitems > :nth-child(1) > :nth-child(1) > #orderitemsc0 > .modal > .animation-content > .card > .card-content > :nth-child(1) > form > .materials-modal-footer > .is-primary')
            .click()
        //Order ID ????
        cy.get('[style=""] > :nth-child(1) > :nth-child(1) > :nth-child(1) > form > .vue-form-generator > :nth-child(6) > :nth-child(1) > .field-wrap > :nth-child(1) > .field > .control > .input')
            .type('Teste João')
            .should('have.value', 'Teste João')
        //Total Qnt
        cy.get('[style=""] > :nth-child(1) > :nth-child(1) > :nth-child(1) > form > .vue-form-generator > :nth-child(6) > :nth-child(2) > .field-wrap > :nth-child(1) > .field > .control > .input')
            .type('55')
            .should('have.value', '55')
        //Full Name
        cy.get('[style=""] > :nth-child(1) > :nth-child(1) > :nth-child(1) > form > .vue-form-generator > :nth-child(10) > .form-group > .field-wrap > :nth-child(1) > .field > .control > .input')
            .type('Teste João')
            .should('have.value', 'Teste João')
        // Tax Number
            cy.get('[style=""] > :nth-child(1) > :nth-child(1) > :nth-child(1) > form > .vue-form-generator > :nth-child(11) > .form-group > .field-wrap > :nth-child(1) > .field > .control > .input')
            .type('555333222')
            .should('have.value', '555333222')
        //Contact Person 
        cy.get('[style=""] > :nth-child(1) > :nth-child(1) > :nth-child(1) > form > .vue-form-generator > :nth-child(12) > .form-group > .field-wrap > :nth-child(1) > .field > .control > .input')
            .type('Teste João')
            .should('have.value', 'Teste João')
        //Number Contact
        cy.get('[style=""] > :nth-child(1) > :nth-child(1) > :nth-child(1) > form > .vue-form-generator > :nth-child(13) > .form-group > .field-wrap > :nth-child(1) > .field > .control > .input')
            .type('911000111')
            .should('have.value', '911000111')
        //Email
        cy.get('[style=""] > :nth-child(1) > :nth-child(1) > :nth-child(1) > form > .vue-form-generator > :nth-child(14) > .form-group > .field-wrap > :nth-child(1) > .field > .control > .input')
            .type('teste_jv.mail@teste.com')
            .should('have.value', 'teste_jv.mail@teste.com')
        // Secção Address
        //Street and door number
        cy.get('[data-cy=StrtDoorNum]').eq(1)
            .type('Rua das Laranjeiras, 48')
            .should('have.value', 'Rua das Laranjeiras, 48')
        //Floor
        cy.get('[data-cy=FloorFld]').eq(1)
            .type('2º D')
            .should('have.value', '2º D')
        //Zip Code
        cy.get('[data-cy=ZipCodeFld]').eq(1)
            .type('2430-001')
            .should('have.value', '2430-001')
        //City
        cy.get('[data-cy=CityFld]').eq(1)
            .type('Lisbon')
            .should('have.value', 'Lisbon')
        //Country 
        cy.get('[data-cy=CountryFld]').eq(1)
            .should('have.value', 'PT')
        //Additional Information 
        cy.get('[style=""] > :nth-child(1) > :nth-child(1) > :nth-child(1) > form > .vue-form-generator > :nth-child(22) > .form-group > .field-wrap > span > .field > .control > .textarea')
            .type('Isto é um teste. 1234@blablabla$#""!%&ºª~ç')
            .should('have.value', 'Isto é um teste. 1234@blablabla$#""!%&ºª~ç')
        //CheckBox
        cy.get('[style=""] > :nth-child(1) > :nth-child(1) > :nth-child(1) > form > .vue-form-generator > :nth-child(23) > .field-check > .field-wrap > .text-left > :nth-child(1) > .field > .b-checkbox > .check')
            .click()
        //Next Step
        cy.get('[style=""] > :nth-child(1) > :nth-child(1) > :nth-child(1) > form > :nth-child(2) > .is-primary > span')
            .should('have.text', 'Next Step')
            .click()

        // Secção Review
        cy.wait(3000)
        //CheckBox Terms
        cy.get('[style=""] > :nth-child(1) > :nth-child(1) > :nth-child(1) > form > .vue-form-generator > .is-grouped > .field-check > .field-wrap > .text-left > :nth-child(1) > .field > .b-checkbox > .check')
            .click()
        //Button Submit
        cy.get('[style=""] > :nth-child(1) > :nth-child(1) > :nth-child(1) > form > :nth-child(2) > .is-primary > span')
            .should('have.text','Confirm and Submit')
            .click()
        // Secção Submitted
        cy.wait(1000)
        //Validation
        cy.get('[style=""] > .card > .card-content > span > form > .vue-form-generator > :nth-child(1) > .form-group > .field-wrap > :nth-child(1) > .landingMainTitle')
            .should('have.text', 'Order Submitted')
    })
    
})