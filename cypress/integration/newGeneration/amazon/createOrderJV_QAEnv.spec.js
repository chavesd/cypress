/// <reference types="cypress" />

before(() => {

    cy.visit(`${Cypress.env('qaUrl')}`)
    cy.title()
        .should('be.equal', 'circul8® - Home of Circular Economy Efficiency')

})

describe('Order', () => {

    it('Create Order', () => {
        // Next Step
        cy.get('[style=""] > .card > .card-content > :nth-child(1) > form > :nth-child(2) > .button')
            .click()
        cy.wait(3000)
        cy.contains('Order Details')
        //Materials
        cy.get('[style=""] > .card > .card-content > :nth-child(1) > form > .vue-form-generator > :nth-child(3) > .form-group > .field-wrap > #orderitems > .button')
            .click()
        // Categorie
        cy.get('[style=""] > :nth-child(1) > :nth-child(1) > :nth-child(1) > :nth-child(1) > :nth-child(1) > :nth-child(3) > .field-array > :nth-child(2) > #orderitems > :nth-child(1) > :nth-child(1) > #orderitemsc0 > .modal > .animation-content > .card > .card-content > :nth-child(1) > form > :nth-child(1) > :nth-child(1) > .vue-form-generator > fieldset > .field-awesome > .field-wrap > :nth-child(1) > .field > .control > .select > select')
            .select('23')
            .should('have.value', '23')
        //Quantity
        cy.get('[style=""] > :nth-child(1) > :nth-child(1) > :nth-child(1) > :nth-child(1) > :nth-child(1) > :nth-child(3) > .field-array > :nth-child(2) > #orderitems > :nth-child(1) > :nth-child(1) > #orderitemsc0 > .modal > .animation-content > .card > .card-content > :nth-child(1) > form > :nth-child(1) > :nth-child(1) > .vue-form-generator > fieldset > .field-abstract > .field-wrap > :nth-child(1) > .field > .control > .input')
            .type('20')
            .should('have.value', '20')
        //Description
        cy.get('[style=""] > :nth-child(1) > :nth-child(1) > :nth-child(1) > :nth-child(1) > :nth-child(1) > :nth-child(3) > .field-array > :nth-child(2) > #orderitems > :nth-child(1) > :nth-child(1) > #orderitemsc0 > .modal > .animation-content > .card > .card-content > :nth-child(1) > form > :nth-child(1) > :nth-child(1) > .vue-form-generator > fieldset > .field-textarea > .field-wrap > span > .field > .control > .textarea')
            .type('Teste João')
            .should('have.value', 'Teste João')
        // Button update
        cy.get('[style=""] > :nth-child(1) > :nth-child(1) > :nth-child(1) > :nth-child(1) > :nth-child(1) > :nth-child(3) > .field-array > :nth-child(2) > #orderitems > :nth-child(1) > :nth-child(1) > #orderitemsc0 > .modal > .animation-content > .card > .card-content > :nth-child(1) > form > .materials-modal-footer > .is-primary')
            .click()

        //Materials 2
        cy.get('[style=""] > .card > .card-content > :nth-child(1) > form > .vue-form-generator > :nth-child(3) > .form-group > .field-wrap > #orderitems > .button')
            .click()
        // Categorie
        cy.get('[style=""] > :nth-child(1) > :nth-child(1) > :nth-child(1) > :nth-child(1) > :nth-child(1) > :nth-child(3) > .field-array > :nth-child(2) > #orderitems > :nth-child(2) > :nth-child(1) > #orderitemsc1 > .modal > .animation-content > .card > .card-content > :nth-child(1) > form > :nth-child(1) > :nth-child(1) > .vue-form-generator > fieldset > .field-awesome > .field-wrap > :nth-child(1) > .field > .control > .select > select')
            .select('23')
            .should('have.value', '23')
        //Quantity
        cy.get('[style=""] > :nth-child(1) > :nth-child(1) > :nth-child(1) > :nth-child(1) > :nth-child(1) > :nth-child(3) > .field-array > :nth-child(2) > #orderitems > :nth-child(2) > :nth-child(1) > #orderitemsc1 > .modal > .animation-content > .card > .card-content > :nth-child(1) > form > :nth-child(1) > :nth-child(1) > .vue-form-generator > fieldset > .field-abstract > .field-wrap > span > .field > .control > .input')
           .type('20')
            .should('have.value', '20')
        //Description
        cy.get('[style=""] > :nth-child(1) > :nth-child(1) > :nth-child(1) > :nth-child(1) > :nth-child(1) > :nth-child(3) > .field-array > :nth-child(2) > #orderitems > :nth-child(2) > :nth-child(1) > #orderitemsc1 > .modal > .animation-content > .card > .card-content > :nth-child(1) > form > :nth-child(1) > :nth-child(1) > .vue-form-generator > fieldset > .field-textarea > .field-wrap > span > .field > .control > .textarea')
            .type('Teste João')
            .should('have.value', 'Teste João')
        // Button update
        cy.get('[style=""] > :nth-child(1) > :nth-child(1) > :nth-child(1) > :nth-child(1) > :nth-child(1) > :nth-child(3) > .field-array > :nth-child(2) > #orderitems > :nth-child(2) > :nth-child(1) > #orderitemsc1 > .modal > .animation-content > .card > .card-content > :nth-child(1) > form > .materials-modal-footer > .is-primary')
            .click()


        // Order ID 
        cy.get('[style=""] > :nth-child(1) > :nth-child(1) > :nth-child(1) > form > .vue-form-generator > :nth-child(7) > .form-group > .field-wrap > span > .field > .control > .input')    
            .type('Teste João')
            .should('have.value', 'Teste João')
        // Total
        cy.get('[style=""] > :nth-child(1) > :nth-child(1) > :nth-child(1) > form > .vue-form-generator > :nth-child(8) > .form-group > .field-wrap > span > .field > .control > .input')
            .type('55')
            .should('have.value', '55')
        //Full Name
        cy.get('[style=""] > :nth-child(1) > :nth-child(1) > :nth-child(1) > form > .vue-form-generator > :nth-child(12) > .form-group > .field-wrap > span > .field > .control > .input')
            .type('Teste João')
            .should('have.value', 'Teste João')
        //Tax Number
        cy.get('[style=""] > :nth-child(1) > :nth-child(1) > :nth-child(1) > form > .vue-form-generator > :nth-child(13) > .form-group > .field-wrap > span > .field > .control > .input')
            .type('555333222')
            .should('have.value', '555333222')
        // Contact Person
        cy.get('[style=""] > :nth-child(1) > :nth-child(1) > :nth-child(1) > form > .vue-form-generator > :nth-child(14) > .form-group > .field-wrap > span > .field > .control > .input')
            .type('Teste João')
            .should('have.value', 'Teste João')
        // Phone Number
        cy.get('[style=""] > :nth-child(1) > :nth-child(1) > :nth-child(1) > form > .vue-form-generator > :nth-child(15) > .form-group > .field-wrap > span > .field > .control > .input')
           .type('911000111')
            .should('have.value', '911000111')
        //Email
        cy.get('[style=""] > :nth-child(1) > :nth-child(1) > :nth-child(1) > form > .vue-form-generator > :nth-child(16) > .form-group > .field-wrap > span > .field > .control > .input')
            .type('teste_jv.mail@teste.com')
            .should('have.value', 'teste_jv.mail@teste.com')

        // Secção Address
        //Street
        cy.get('[style=""] > :nth-child(1) > :nth-child(1) > :nth-child(1) > form > .vue-form-generator > :nth-child(20) > .form-group > .field-wrap > span > .field > .control > .input')
           .type('Rua das Laranjeiras, 48')
            .should('have.value', 'Rua das Laranjeiras, 48')
        //Flor
        cy.get('[style=""] > :nth-child(1) > :nth-child(1) > :nth-child(1) > form > .vue-form-generator > :nth-child(21) > :nth-child(1) > .field-wrap > span > .field > .control > .input')
           .type('2º D')
            .should('have.value', '2º D')
        //Zip Code
        cy.get('[style=""] > :nth-child(1) > :nth-child(1) > :nth-child(1) > form > .vue-form-generator > :nth-child(21) > :nth-child(2) > .field-wrap > span > .field > .control > .input')
           .type('2430-001')
            .should('have.value', '2430-001')
        //City
        cy.get('[style=""] > :nth-child(1) > :nth-child(1) > :nth-child(1) > form > .vue-form-generator > :nth-child(22) > .field-abstract > .field-wrap > span > .field > .control > .input')
            .type('Lisbon')
            .should('have.value', 'Lisbon')

            /*cy.get('[style=""] > :nth-child(1) > :nth-child(1) > :nth-child(1) > form > .vue-form-generator > :nth-child(20) > .disabled > .field-wrap > :nth-child(1) > .field > .control > .select > select')
            .should('have.value', 'PT')*/
        //Additional information
        cy.get('[style=""] > :nth-child(1) > :nth-child(1) > :nth-child(1) > form > .vue-form-generator > :nth-child(24) > .form-group > .field-wrap > span > .field > .control > .textarea')
            .type('Isto é um teste. 1234@blablabla$#""!%&ºª~ç')
            .should('have.value', 'Isto é um teste. 1234@blablabla$#""!%&ºª~ç')

        cy.get('[style=""] > :nth-child(1) > :nth-child(1) > :nth-child(1) > form > .vue-form-generator > :nth-child(25) > .field-check > .field-wrap > .text-left > :nth-child(1) > .field > .b-checkbox > .check')
               .click()

        cy.get('[style=""] > :nth-child(1) > :nth-child(1) > :nth-child(1) > form > :nth-child(2) > .is-primary > span')
            .should('have.text', 'Next Step')
            .click()

        // Secção Review
        cy.wait(3000)
        cy.get('[style=""] > :nth-child(1) > :nth-child(1) > :nth-child(1) > form > .vue-form-generator > .is-grouped > .field-check > .field-wrap > .text-left > :nth-child(1) > .field > .b-checkbox > .check')
            .click()

        cy.get('[style=""] > :nth-child(1) > :nth-child(1) > :nth-child(1) > form > :nth-child(2) > .is-primary > span')
            .should('have.text','Confirm and Submit')
            .click()

            // Secção Submitted
        cy.wait(3000)
        cy.get('[style=""] > .card > .card-content > span > form > .vue-form-generator > :nth-child(1) > .form-group > .field-wrap > :nth-child(1) > .landingMainTitle')
            .should('have.text', 'Order Submitted')
    })
    
})
