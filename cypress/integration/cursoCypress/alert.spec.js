/// <reference types="cypress" />

describe('Work with alerts', () => {
    //Será executado apenas uma vez, antes dos testes
    before(() => {
        cy.visit('https://wcaquino.me/cypress/componentes.html')
    })
    //Será executado antes de cada teste
    beforeEach(() => {
        cy.reload()
    })

    it('Alert', () => {
        cy.get('#alert').click()
        cy.on('windon:alert', msg => {
            console.log(msg)
            expect(msg).to.be.equal('Alert Simples')
        })
    })

    it('Alert with mock', () => {

        const stub = cy.stub().as('alerta')
        cy.on('windon:alert', stub)
        cy.get('#alert').click().then(() => {
            expect(stub.getCall(0)).to.be.calledWith('Alert Simples')
        })

    })

    it('Confirm', () => {
        cy.on('windon:confirm', msg => {
            expect(msg).to.be.equal('Confirm Simples')
        })
        cy.on('windon:alert', msg => {
            expect(msg).to.be.equal('Confirmado')
        })
        cy.get('#confirm').click()
    })

    it('Deny', () => {
        cy.on('windon:confirm', msg => {
            expect(msg).to.be.equal('Confirm Simples')
            return false
        })
        cy.on('windon:alert', msg => {
            expect(msg).to.be.equal('Negado')
        })
        cy.get('#confirm').click()
    })

    
    it('Prompt', () => {
        cy.window().then(win => {

            cy.stub(win, 'prompt').returns('42')
        })

         cy.on('windon:confirm', msg => {
             expect(msg).to.be.equal('Era 42?')
         })
        cy.on('windon:alert', msg => {
            expect(msg).to.be.equal(':D')
        })

        cy.get('#prompt').click()
    })

    it.only('Validando mensagens', () => {
        const stub = cy.stub().as('alerta')
        cy.on('window:alert', stub)
        cy.get('#formCadastrar').click()
            .then(() => expect(stub.getCall(0)).to.be.calledWith('Nome eh obrigatorio'))

        cy.get('#formNome').type('Debora')
        cy.get('#formCadastrar').click()
            .then(() => expect(stub.getCall(1)).to.be.calledWith('Sobrenome eh obrigatorio'))

        
        cy.get('[data-cy=dataSobrenome]').type('Chaves')
        cy.get('#formCadastrar').click()
            .then(() => expect(stub.getCall(2)).to.be.calledWith('Sexo eh obrigatorio'))

        cy.get('#formSexoMasc').click()
        cy.get('formCadastrar').click()
        
        cy.get('#resultado > :nth-child(1)').should('contain', 'Cadastrado')

    })



})