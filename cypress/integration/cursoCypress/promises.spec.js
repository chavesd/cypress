it('sem testes, ainda', () => { })

/* const getSmething = (callback) => {
    setTimeout( () => {
       callback(12);
    }, 1000)
}
    
const system = () => {
    console.log('init');
    getSmething(some => console.log(`Something is ${some}`);
    console.log('end')
}

system(); */




/* const getSmething = () => {
    return new Promise((resolve, reject) => {
        setTimeout(() => {
            resolve(13);
         }, 1000)
    })

}
    
const system = async () => {
    console.log('init');
    const some  = await getSmething()
    console.log(`Something is ${some}`)
    console.log('end')
}

system(); */

const getSmething = () => {
    return new Promise((resolve, reject) => {
        setTimeout(() => {
            resolve(13);
         }, 1000)
    })

}
    
const system = () => {
    console.log('init');
    const prom = getSmething();
    prom.then(some => {
        console.log(`Something is ${some}`);
    })
    console.log('end')
}

system();