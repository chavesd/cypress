/// <reference types="cypress" />

describe('Esperas...', () => {

    //Será executado apenas uma vez, antes dos testes
    before(() => {
        cy.visit('https://wcaquino.me/cypress/componentes.html')
    })
    //Será executado antes de cada teste
    beforeEach(() => {
        cy.reload()
    })

    it('Deve aguardar elemento ficar disponivel', () => {

    cy.get('#novoCampo').should('not.exist')
    cy.get('#buttonDelay').click()
    cy.get('#novoCampo').should('not.exist')
    cy.get('#novoCampo').should('exist')
    cy.get('#novoCampo').type('funciona')

    })
    
    it('Deve fazer retrys', () => {
    
        cy.get('#buttonDelay').click()
        
        cy.get('#novoCampo')
            .should('exist')
            .type('funciona')
    
    })

    it('Uso do find', () => {

        cy.get('#buttonList').click()
        cy.get('#lista li')
            .find('span')
            .should('contain', 'Item 1')
        cy.get('#lista li span')
            .should('contain', 'Item 2')

    })

    it('Uso do timeout', () => {

        // cy.get('#buttonDelay').click()
        //cy.get('#novoCampo', { timeout: 1000}).should('exist')
        // cy.get('#novoCampo').should('exist')

        // cy.get('#buttonListDOM').click()
        // //cy.walt(5000)
        // cy.get.prototype('#lista li span', {timeout: 3000})
        //     .should('contain', 'Item 2')

        cy.get('#buttonListDOM').click()
        //cy.walt(5000)
        cy.get.prototype('#lista li span')
            .should('have.length', '1')
        cy.get.prototype('#lista li span')
            .should('have.length', '2')

    })

    it.only('Click Retry', () => {

        cy.get('#buttonCount')
            .click()
            .click()
            .should('have.value', '1')
    })

        // so quem considera o return é a tag then, não funciona com o should

        it.only('Should vs Then', () => {
            cy.get('#buttonListDOM').click()
            cy.get('#Lista li span').then($el =>{
    
            //.should('have.length', 1)
            //console.log($el)
            expect($el).to.have.length(1)
            return 2
             })//.and('eq', 2)
            //     .and('have.id', 'buttonListDOM')
        })
})